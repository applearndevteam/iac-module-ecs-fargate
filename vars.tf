variable "container_port" {}
variable "name" {}
variable "vpc_id" {}
variable "subnets" {
  type = list(string)
}
variable "ecr_registry" {}
variable "container_tag" {
  default = "latest"
}
variable "allowed_ips" {
  type = list(map(string))
}
variable "environment" {
  type = list(map(string))
  default = []
}
variable "secrets" {
  type = list(map(string))
  default = []
}
variable "internal_lb" {
  type = bool
  default = false
}
variable "assign_public_ip" {
  type = bool
  default = false
}
variable "lb_port" {}
variable "lb_protocol" {}
variable "lb_health_check_path" {
  default = "/"
}
variable "container_protocol" {}
variable "ssl_cert_arn" {
  default = null
}
variable "cpu" {
  default = 256
}
variable "memory" {
  default = 512
}
variable "http_redirect" {
  type = bool
  default = false
}
