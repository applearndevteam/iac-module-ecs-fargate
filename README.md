
# iac-module-ecs-fargate

Terraform module which creates [AWS ECS Fargate infrastructure](https://aws.amazon.com/fargate/) on AWS. Includes a Load Balancer with optional SSL termination.

**Note: requires `terraform >= 0.12.0`**
## Usage

```
module "ecs-cluster" {
  source = "git::https://bitbucket.org/applearndevteam/iac-module-ecs-fargate.git"
  name = "example-cluster"
  vpc_id = "vpc-12345678"
  subnets = ["subnet-12345678", "subnet-9101112"]
  container_port = 80
  container_protocol = "HTTP"
  lb_port = 80
  lb_protocol = "HTTP"
  ecr_registry = "1234567891011.dkr.ecr.us-east-1.amazonaws.com/example-container"
  container_tag = "latest"
  allowed_ips = [
    {
      cidr = "1.2.3.4/32"
      port = 80
    },
    {
      cidr = "5.6.7.8/32"
      port = 443
    }
  ]
  environment = [
    {
      name = "string"
      value = "string"
    },
    {
      name = "my"
      value = "env-variable"
    }
  ]
  secrets = [
    {
      name = "environment_variable_name"
      valueFrom = "arn:aws:ssm:region:aws_account_id:parameter/parameter_name"
    }
  ]
}
```


## Inputs

|Name|Description|Type|Default|Required|
|---|---|---|---|---|
|name|Name of ECS cluster and load balancer to create. E.g `"my-example-container-cluster"`|string|n/a|**yes**|
|vpc_id|VPC ID to create infrastructure in|string|n/a|**yes**|
|subnets|Subnet IDs to create infrastructure in|list|n/a|**yes**|
|container_port|Port container listens on|number|n/a|**yes**|
|container_protocol|Protocol container uses|string|n/a|**yes**|
|lb_port|Port LB listens on|number|n/a|**yes**|
|lb_protocol|Protocol LB uses|string|n/a|**yes**|
|ecr_registry|ECR Registry URL where container is located|string|n/a|**yes**|
|allowed_ips|Map of allowed subnets and ports to whitelist, see example above|list(map(string))|n/a|**yes**|
|lb_health_check_path|Path LB should use to check health of container|string|"/"|no|
|container_tag|Container tag to deploy|string|"latest"|no|
|ssl_cert_arn|ARN of SSL certificate to use if terminating SSL at the load balancer|string|null|no|
|cpu|CPU value for container to use|number|256|no|
|memory|Memory value for container to use|number|512|no|
|environment|Environment variables to pass to the container, list of maps. See example above|list(map(string))|[]|no|
|secrets|Secrets from AWS secrets manager passed to the container as environment variables, list of maps. See example above|list(map(string))|[]|no|
|internal_lb|Whether Load Balancer should be internal or public facing|bool|false|no|
|http_redirect|Whether Load Balancer should redirect HTTP traffic to HTTPS|bool|false|no|
