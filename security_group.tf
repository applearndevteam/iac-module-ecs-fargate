resource "aws_security_group" "ecs" {
  name        = var.name
  vpc_id      = var.vpc_id

  dynamic "ingress" {
    for_each = var.allowed_ips
    content {
      from_port = ingress.value["port"]
      to_port   = ingress.value["port"]
      protocol  = "tcp"
      cidr_blocks     = [ingress.value["cidr"]]
    }
  }

  ingress {
    self = true
    from_port = var.container_port
    to_port   = var.container_port
    protocol  = "tcp"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}
