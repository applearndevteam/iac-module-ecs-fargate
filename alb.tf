resource "aws_alb" "ecs" {
  name = var.name
  security_groups = [aws_security_group.ecs.id]
  subnets         = var.subnets
  internal        = var.internal_lb
}

resource "aws_alb_target_group" "ecs" {
  name = var.name
  port        = var.container_port
  protocol    = var.container_protocol
  vpc_id      = var.vpc_id
  target_type = "ip"
  deregistration_delay = 10

  health_check {
    path = var.lb_health_check_path
    interval = 5
    healthy_threshold = 2
    timeout = 2
  }
}

resource "aws_alb_listener" "ecs" {
  load_balancer_arn = aws_alb.ecs.id
  port              = var.lb_port
  protocol          = var.lb_protocol
  certificate_arn   = var.ssl_cert_arn
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"

  default_action {
    target_group_arn = aws_alb_target_group.ecs.id
    type             = "forward"
  }
}

resource "aws_alb_listener" "http_redirect" {
  count = var.http_redirect == true ? 1 : 0
  load_balancer_arn = aws_alb.ecs.id
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

output "lb_dns" {
  value = aws_alb.ecs.dns_name
}
