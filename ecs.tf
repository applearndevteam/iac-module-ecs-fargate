resource "aws_ecs_cluster" "cluster" {
  name = var.name
}

resource "aws_ecs_task_definition" "task_definition" {
  family                = var.name
  container_definitions = templatefile("${path.module}/task-definitions/service.json", {
    name = var.name,
    container_port = var.container_port,
    ecr_registry = var.ecr_registry,
    container_tag = var.container_tag,
    environment = jsonencode(var.environment)
    secrets = jsonencode(var.secrets)
    aws_region = data.aws_region.current.name
  })
  requires_compatibilities = ["FARGATE"]
  network_mode = "awsvpc"
  cpu = var.cpu
  memory = var.memory
  execution_role_arn = aws_iam_role.ecs_execution.arn
  lifecycle {create_before_destroy=true}
}

resource "aws_ecs_service" "service" {
  name            = var.name
  cluster         = aws_ecs_cluster.cluster.id
  task_definition = aws_ecs_task_definition.task_definition.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    subnets = var.subnets
    security_groups = [aws_security_group.ecs.id]
    assign_public_ip = var.assign_public_ip
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.ecs.arn
    container_name   = var.name
    container_port   = var.container_port
  }
}
